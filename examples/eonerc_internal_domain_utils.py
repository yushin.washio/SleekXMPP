import logging
import ssl
from sleekxmpp.xmlstream import cert

VALID_HOST_XMPP_ADDRS = (
    '137.226.248.63',
    '*.137.226.248.63',
    '137.226.248.35',
    '*.137.226.248.35',
    )

ENCRYPTED_MSG_BODY = 'This message is encrypted.'

def uid_decompose(uid):
    name_real = uid
    name_email = None
    name_email_end = uid.rfind('>')
    name_real_end = uid.rfind('<', 0, name_email_end)
    if name_email_end >= 0 and name_real_end >= 0:
        name_real = uid[:name_real_end]
        name_email = uid[name_real_end+1:name_email_end]
    return {'Name-Real': name_real.strip(), 'Name-Email': name_email}

def apply_eonerc_certs(ClientXMPPClass):
    """
    call this for any sleekxmpp.ClientXMPP (child) class
    to get a child class accepting EONERC internal certificates
    """
    class EonercClientXMPP(ClientXMPPClass):
        def __init__(self, jid, password, use_pgp=False, *args, **kwargs):
            ClientXMPPClass.__init__(self, jid, password, *args, **kwargs)
            self.use_pgp = use_pgp

            # Using a EONERC internal domain, the certificate
            # does not contain the custom domain, just the OpenFire
            # server location. So we will need to process invalid
            # certifcates ourselves and check that it really
            # is from the EONERC network.
            self.add_event_handler("ssl_invalid_cert", self.invalid_cert)

            if use_pgp:
                self.register_plugin('xep_0027') # OpenPGP

        def start(self, event):
            if self.use_pgp:
                self.find_set_key(generate=True, secret=True)
            ClientXMPPClass.start(self, event)

        def invalid_cert(self, pem_cert):
            der_cert = ssl.PEM_cert_to_DER_cert(pem_cert)
            errs = []
            for expected in VALID_HOST_XMPP_ADDRS:
                try:
                    cert.verify(expected, der_cert)
                    logging.debug(
                        "CERT: Found EONERC internal certificate %s", expected)
                    return
                except cert.CertificateError as err:
                    errs.append(err)
            logging.debug(
                'CERT_EXTRACTED_NAMES: %s', cert.extract_names(der_cert))
            cert.log.error(repr(errs))
            self.disconnect(send_close=False)

        def find_set_key(self, jid=None, generate=False, secret=False):
            pgp_plugin = self.plugin['xep_0027']
            if not pgp_plugin.get_keyid(jid=jid):
                gpg = pgp_plugin.gpg
                from_keyring = gpg.list_keys(secret=secret)
                logging.debug('list_keys(): %s', repr(from_keyring))
                keyid = jid.bare if hasattr(jid, 'bare') else (
                    jid if isinstance(jid, str) else self.jid)
                match = [key['fingerprint'] for key in from_keyring if any(
                    uid_decompose(uid)['Name-Email'] == keyid
                    for uid in key['uids'])]
                logging.debug('match for %s: %s', keyid, repr(match))
                if len(match) == 0:
                    logging.debug('uid_decompose: %s', repr(
                        uid_decompose(key['uids'][0]) for key in from_keyring))
                    if generate:
                        if jid is not None and jid != self.jid:
                            logging.warning(
                                'Generating a key for %s (not myself)!', jid)
                        newkey = gpg.gen_key(gpg.gen_key_input(
                            name_email=keyid))
                    else:
                        raise RuntimeError(
                            'Key not found and will not be generated for {:s}!'
                            .format(jid))
                if hasattr(jid, 'jid'):
                    jid = jid.jid
                pgp_plugin.set_keyid(jid=jid, keyid=keyid)

        def make_encrypted_message(
                self, mto, mencrypted=None, mbody=ENCRYPTED_MSG_BODY,
                *args, **kwargs):
            msg = self.make_message(mto=mto,
                                    mbody=mbody,
                                    *args, **kwargs)
            msg['encrypted'] = mencrypted
            return msg

        def send_encrypted_message(self, *args, **kwargs):
            msg = self.make_encrypted_message(*args, **kwargs)
            logging.debug(msg.__dict__)
            msg.send()

    return EonercClientXMPP

def reply_encrypted(msg, mencrypted=None, *args, **kwargs):
    reply_msg = msg.reply(ENCRYPTED_MSG_BODY, *args, **kwargs)
    reply_msg['encrypted'] = mencrypted
    return reply_msg
